#!/usr/bin/env python3
# coding: utf-8

AUTHOR = u'Simon Descarpentries'
SITENAME = u'Meta-Press.es'
SITESUBTITLE = u'Decentralized search engine & automatized press reviews'
DESCRIPTION = u'Meta-Press.es : decentralized search engine and automatized press reviews'
SITEURL = 'https://www.meta-press.es'
PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_PAGINATION = 10
FEED_ALL_ATOM = 'flux/all.atom.xml'  # Feed generation is usually not desired when developing
# SOCIAL_WIDGET_NAME = 'Flux'
# SOCIAL = (('envelope', 'mailto:siltaar@meta-press.es'), )
RELATIVE_URLS = False  # document-relative URLs when developing, see publishconf.py
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['asciidoc_reader', 'i18n_subsites', 'tipue_search']
ASCIIDOC_CMD = 'asciidoctor'
ASCIIDOC_OPTIONS = ['--section-numbers', '-a icons=font', '-a source-highlighter=pygments',
	'--base-dir=content']
# SLUGIFY_SOURCE = 'basename'
CACHE_CONTENT = True
LOAD_CONTENT_CACHE = True
# 10.88s -> 0.31s
# 23.20s -> 0.67s
OUTPUT_PATH = '/tmp/meta-press.es/output/'
THEME = 'theme/pelican-mp'
# DISPLAY_CATEGORIES_ON_MENU = False
# DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (
	('Index', '/'),
	('Journal', '/category/journal.html'),
	('Revues', '/category/revues.html'),
	('Princip.', '/pages/principles.html'),
	('Documentation', '/pages/meta-press.es_documentation.html'),
	('A propos', '/pages/a_propos.html'),
)
LINKS = (
	('<b>Installation</b> via Mozilla', 'https://addons.mozilla.org/firefox/addon/meta-press-es/',
		'/theme/img/firefox.png'),
	('Sources via Framagit', 'https://framagit.org/Siltaar/meta-press-ext',
		'/theme/img/framagitlab_24.png'),
	('<b>Support</b> via Patreon', 'https://patreon.com/metapress',
		'/theme/img/patreon_24.png'),
	('Support via Liberapay', 'https://liberapay.com/Meta-Press.es',
		'/theme/img/liberapay_yellow_24.png'),
)
I18N_SUBSITES = {  # mapping: language_code -> settings_overrides_dict
	'fr': {
		'SITESUBTITLE': 'Moteur de recherche décentralisé & revue de presse automatisée',
	},
}
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
TAG_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
DIRECT_TEMPLATES = ('index', 'categories', 'archives', 'search', 'tipue_search')
TIPUE_SEARCH_SAVE_AS = 'tipue_search.json'
FAVICON = '/theme/img/favicon-metapress-v2.png'
ARTICLE_URL = '{category}/{date:%Y}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{date:%Y}/{slug}.html'
