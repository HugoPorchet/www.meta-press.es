= Release of Meta-Press.es v1.0 and roadmap 2020
:slug: release_of_meta-press.es_v1.0
:lang: en
:date: 2019-10-30
:author: Siltaar

Mozilla just approved Meta-Press.es v1.0 in addons.mozilla.org. It's now
possible to *install this add-on in Firefox*, in a
https://addons.mozilla.org/en-US/firefox/addon/meta-press-es/[few clicks].

This release includes the following improvements over the initially published
link:/journal/2017/motivations.html[proof of concept] :

- It supports the selection of which newspapers to search in, via filters, that
  are saved in the Firefox add-on storage between two usages ;
- It supports exporting and importing results (or selection of results) via a
  .JSON file ;
- *It supports exporting results* (or *selection*) *in RSS* or ATOM ;
- It supports 50 sources, 10% of them brought by contributors ;
- It supports defining new sources from the setting panel.

And a lot of small technical improvements : such as DNS prefetch footnote:[By
the way, I'm looking for a way to verify that it works, because the Network tab
of the Firefox developers tool don't show DNS queries], waiting animation
during queries footnote:[I hacked a .gif in the Gimp to get it slower, but it
end up badly rendered, I need help on this also…], CSS fixes, sub-search
efficiency…

So I covered the points : 2. ; 3. ; 4. ; ½ 5. ; ½ 6 plus the issues listed in
the introduction of my link:/journal/2017/roadmap_2018.html[previous roadmap]
and the promises of my excerpt are now met.

*In the future*, we might focus on
link:/journal/2019/first_contributed_newspapers.html[growing the source base] :
200, 2000, 20 000 ? Let's discover how many sources can be defined with the
current system, with instant tag filtering as it feels now.

The checking suite to maintain all those sources (my point 1. in the previous
roadmap) would already be useful with 50 sources footnote:[As 7 are already
reported as broken.] and will become the mandatory next thing to implement.

To finish, if a broad audience comes to this work, internationalisation (i18n)
of the add-on would become an interesting feature, even if it's already
possible to search in some french newspapers, and virtually possible to define
newspapers from every language, using
link:/journal/2019/month_nb-released.html[month_nb] to convert dates.

image::/images/20191029_meta-press_europe_selection-mode_crop_640.png[link="/images/20191029_meta-press_europe_selection-mode_crop.png", title="20191028 screenshot of the selection mode, click to enlarge the picture."]
