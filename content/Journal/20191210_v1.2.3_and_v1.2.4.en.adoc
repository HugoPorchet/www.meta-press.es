= Release of Meta-Press.es v1.2.4
:slug: release_of_meta-press.es_v1.2.4
:lang: en
:date: 2019-12-10
:author: Siltaar

The versions 1.2.3 and 1.2.4 have been released.

*v1.2.3* introduced a better responsive design and compatibility with Firefox
for Android. There were other presentation improvements such as:

- removal of excerpt inline style attributes,
- fixed checkboxes position in selection mode…

*v1.2.4* introduced bug fixes and 13 more sources:

- 2 of them were *contributed by Gabriel d'Atri from Uruguay*, and more may
  come.
- the other were mainly from the
  https://fr.wikipedia.org/wiki/Presse_de_r%C3%A9f%C3%A9rence[Presse de
  référence] Wikipedia article. More will come in the next releases.
- live fetch stats has been moved to the top, under the query input, to avoid
  scrolling all the time to see what happens
- use of zopfli zip compression and image for title (to avoid embedding fonts)
  to spare 100ko for the Meta-Press.es archive downloaded during installation
  (comparisons were conducted between zip, 7zip zip and Zopfli Zip formats ;
  sticking to pure `zip -9` would have been enough).
- a discussion have been opened about
  https://framagit.org/Siltaar/meta-press-ext/issues/33[permissions] to find
  the best way to reduce the permissions required by Meta-Press.es, as the
  ability to fine tune this matter appears and Firefox became talkative about
  it during addon installation
- a list of
  https://framagit.org/Siltaar/meta-press-ext/wikis/Incompatible-sources[incompatible
  sources] is now available in the wiki of the project. Everyone wanting to
  help be without technical skills is encouraged to contact sources of this
  list with no feedback yet to try to have them fixing their search features.
- the link:/pages/meta-press.es_documentation.html[official documentation] have
  been augmented, with screenshots and more explanations about how to add
  sources. It is now available on the main website and is also available
  in french
- A link:/category/revues.html[Revues] category feed is now present in the main
  website to track press coverage of Meta-Press.es, for instance
  link:/revues/2019/framasoft_org_meta-press.es_a_ta_disposition.html[Framasoft]
  published an interview about Meta-Press.es in the Framablog.
- Meta-Press.es is currently able to search through: *122 sources for 43
  countries and 21 languages*.
- With the Framablog interview, we're back to hundreds of downloads by day and
  up to 400+ daily users.
- Meta-Press.es also gathered 2 feedbacks at 5 stars.
