= Sortie de Meta-Press.es v1.4
:slug: release_of_meta-press.es_v1.4
:lang: fr
:date: 2020-05-06
:author: Siltaar

Cette version s'est faîte attendre, mais elle permet d'atteindre une première
étape du link:/fr/journal/2020/funds-from-the-nlnet-foundation.html[partenariat
avec la NLnet] : l'internationalisation de l'interface utilisateur (et quelques
paramètres supplémentaires).

L'objectif principal des derniers développements était d'améliorer l'interface
utilisateur, voici les changements les plus remarquables :

- Nouveaux paramètres :
** i18n de l'interface graphique, avec : en, fr, eo footnote:[Traduction en Espéranto par http://esperantolemans.org[Le Groupe Espéranto du Maine].] disponibles
** fond sombre : automatique ou forcé
** chargement des gros titres (ou pas, ou limité)…
- Nouvelles fonctionnalités :
** taille de page du carrousel des gros titres
** conservation des termes de recherche lors d'une annulation de requête
** bouton "+ ajout de source" directement accessible depuis l'interface principale
- Raffinements de l'interface :
** ajout de plus de contraste pour les vieux écrans VGA
** déplacement de la page aux résultats une fois ces derniers arrivés
** lien pour remettre les filtres à zéro
** lien pour faire un retour aux développeurs dans le pieds de page
** import / export des résultats en RSS par défaut (ce format étant le plus célèbre)
- 11 nouvelles sources : AFR, ANSA, France Inter, La Naciòn, Sunday Morning Herald…
** source scientifique : scihub
** source encyclopédique : e-sidoc
** sources cassées entre temps : Der Spiegel, LeTemps.ch
- Taille de l'extension : 201ko zippés footnote:[Il semble toutefois que Mozilla re-compresse l'archive avec un algorithme moins efficace, arrivant à 226.19 KB à télécharger.]
** c'est 50% de la taille de la précédente version, et 33% de la plus grosse (v1.2)
** cette performance est principalement du à un usage extensif des graphics SVG
** et une meilleur compression via zopfli (depuis la v1.2.4) (voir https://www.grimoire-command.es/2019/zip_compression_comparison.html[Zip compression comparison] dans le Grimoire-Command.es)
** l'animation `.gif` (163ko) a été remplacée par 4 lignes de CSS

PS: la v1.4.1 ajoute l'espagnol aux langues de l'interface et corrige un problème de CSS.
