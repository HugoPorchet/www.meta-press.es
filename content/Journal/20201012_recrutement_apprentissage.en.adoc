= Recruitement 2020 OK and v1.5.2
:slug: recrutement_2020
:lang: en
:date: 2020-10-12
:author: Siltaar

Two months after the publication of the initial call, the project Meta-Press.es
is pleased to welcome https://framagit.org/Christo26.g[Christopher Gauthier] as
part-time apprentice. He will work 3 days a week on Meta-Press.es during a
year.

But a good news rarely comes alone, and he will be helped, during a maximum of
6 months, by https://framagit.org/Remsi[Rémi Plancher] recruited as part-time
trainee, thanks to another exceptional disposition of the government allowing
a candidate for part-time learning years to start with by 6 months as a
voluntary trainee in a company, while searching for an apprentice contract.

Both started to work on Meta-Press.es adding new sources, lifting the numbers
up to 200 sources, for 53 countries and 22 languages.

The *v1.5.2* of Meta-Press.es, published today, also add some features in the
source description format, such as :

- the possibility to declare a charset for sources not serving pages as UTF8
	(it took 200 sources to find one… DailyNK) ;
- the explicit definition of a domain name part for sources needing a
	sub-folder in their relatives URL (DailyNK) ;
- the declaration of redirection URLs, allowing to ask for the corresponding
	host permissions, for sources needing to bounce by a third party domain in
	order to serve results (Daily Telegraph).

Some
https://www.meta-press.es/pages/meta-press.es_documentation.html#_redirections[documentation]
have been added and some more are still in the redaction process.

This version puts aside the update notification mechanism for the moment. It
was relying on the `localStorage` API, and this last one is wiped out at each
end of browser session in private mode (which is the default on TorBrowser).
This was triggering the opening of the Meta-Press.es website at each new
session… Sorry for the inconvenience. This functionality will come back based
on the `browser.storage` API, which is kept between two browser sessions (at
least still with Firefox 83.0a1).

Then, a work on accessibility have been started. For the moment it is visible
as better contrasts for texts via the
https://addons.mozilla.org/en-US/firefox/addon/wcag-contrast-checker/[WCAG
contrast checker] tool, but numerous recommendations have been formulated by
the https://www.accessibility.nl[Team Accessibility Foundation] after the audit
of Meta-Press.es via https://www.ngi.eu[NGI0].

And to finish, Christopher is currently working this week on the replacement of
the https://github.com/Mobius1/Selectr/[Selectr] library (improved select
multiple) by the https://github.com/jshjohnson/Choices[Choices.js] library
which should solve Android problems for sources selections.
