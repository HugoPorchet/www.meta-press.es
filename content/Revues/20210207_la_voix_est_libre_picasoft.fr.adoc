= [La voix est libre - Picasoft] Meta-press.es, un outil de recherche dans l'actualit&eacute;
:slug: 20210207_picasoft
:lang: fr
:date: 2021-02-07
:author: Siltaar

*Podcast* : https://radio.picasoft.net/co/2021-02-01.html[*Meta-Press.es, un outil de recherche dans l'actualité*]

L'invité : Simon Descarpentries aka https://mamot.fr/@Siltaer[Siltaar], associé d'Acoeuro.com, développeur principal de Meta-Press.es

Principaux points abordés :

 -    Pourquoi ne plus vouloir utiliser Google Actualités ? Quel pouvoir ont les moteurs de recherche d'actualité ? Pourquoi l'article 11 ?
 -    Qu'est-ce que méta presse ? En quoi est-ce une bonne réponse ?
 -    Pourquoi développer méta-presse ? En quoi est-il fondamentalement différent de méta press.es ?
 -    Comment utiliser Meta-Press.es
 -    Comment sont choisies les sources disponibles sur Meta press.es
 -    Pourquoi un add-on mozilla ?
 -    Que sont les AMP ? En quoi sont-elles un problème pour les patrons de presse ?
 -    Certains journaux comme le monde sont incompatibles, pourquoi ?
 -    Combien d'utilisateurs sur Méta press.es ?
 -    Comment soutenir méta press.es ?
