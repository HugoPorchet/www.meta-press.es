= [La Nouvelle R&eacute;publique] Deux-S&egrave;vres : il d&eacute;veloppe un moteur de recherche pour la presse
:slug: 20201207_lanouvelle_republique
:lang: fr
:date: 2020-12-07
:author: Siltaar

Développeur web à Pougne-Hérisson, Simon Descarpentries a créé un moteur de
recherche dédié à la presse.

=== « Une solution alternative » au fil d’actualités de Google

[…]  « _De par sa taille, Google a tendance à devenir un serveur centralisé, un
point unique de censure_ », juge Simon Descarpentries. Son système, qu’il
assure « sans pub ni intermédiaire » et qui « fonctionne grâce aux dons »,
*permettrait aux médias de gagner en indépendance vis-à-vis du géant du
numérique*. « Ce peut être leur porte de survie », affirme celui qui est engagé
dans des associations de logiciels libres et de débats participatifs en ligne.
[…]

=== Un projet soutenu par des fonds européens

https://www.lanouvellerepublique.fr/actu/pougne-herisson-il-developpe-un-moteur-de-recherche-pour-la-presse[*Deux-Sèvres : il développe un moteur de recherche pour la presse*]
