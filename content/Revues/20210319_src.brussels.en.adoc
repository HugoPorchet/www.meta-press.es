= [Sources!] Siltaar presents Meta-Press.es
:slug: 20210319_src.brussels_siltaar_presente_meta-press.es
:lang: en
:date: 2021-03-19
:author: Siltaar

*Podcast* : https://src.brussels/Siltaer-presente-meta-press-es.html[Siltaar presents meta-press.es] (_fr_)

https://mamot.fr/@Siltaer[Siltaar] presents us the https://meta-press.es[meta-press.es] WebExtension for Firefox, that he developped starting from the https://www.laquadrature.net[laquadrature] press review needs.

Meta-Press.es ease the search and monitoring of news, querying directly the search feature of newspaper websites (285 sources are available currently), or other (date driven) contents like scientific publications or agendas (https://demosphere.net[Demosphere.net]).

From the 1st version released of Meta-Press.es, it was possible, for tech savvy users to add their own sources to Meta-Press.es, with a dedicated interface in the WebExtension, and the existing sources as example. This task is also well documented.

In addition to browse search results from the differents sources, and present them to the user in chronological order, this WebExtension protects the users against ad-trackers, newspapers and social ones too.

To finish Meta-Press.es is working on https://torproject.org[Tor Browser]!
